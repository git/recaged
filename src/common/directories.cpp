/*
 * ReCaged - a Free Software, Futuristic, Racing Game
 *
 * Copyright (C) 2012, 2013, 2014, 2015, 2023, 2024 Mats Wahlberg
 *
 * This file is part of ReCaged.
 *
 * ReCaged is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ReCaged is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ReCaged.  If not, see <http://www.gnu.org/licenses/>.
 */ 

#include <sys/stat.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

//arbitrarily-sized buffer for some w32 functions
#define W32BUFSIZE 400
//Note: Wont realloc this! I've HAD IT with the crap doze api and its bugs!
//(example: GetModuleFileName on xp/2k: doesn't return buffer-size errors, and
//forgets NULL termination of the string it returns...)

//gets a path from a key->subkey->name value in registry, NULL if fails
static char *RegToPath(HKEY key, const char *subkey, const char *name) {
	char *path = NULL;

	HKEY hKey;
	if (RegOpenKeyExA(key, subkey, 0, KEY_QUERY_VALUE, &hKey) == ERROR_SUCCESS)
	{
		path = new char[W32BUFSIZE];
		unsigned long type;
		unsigned long size=W32BUFSIZE;
		if (RegQueryValueExA(hKey, name, NULL, &type, (LPBYTE) path, &size) == ERROR_SUCCESS)
		{
			if (type == REG_SZ) //should be safe...
			{
				path[W32BUFSIZE-1]='\0'; //just in case registry reading is insane as well...
				//in case reg str ends with one or more (back-)slashes, remove them
				int pos;
				//start at '\0' in end, and go left until last (back)slash
				for (pos=strlen(path); pos>0 && (path[pos-1]=='/' || path[pos-1]=='\\'); --pos);

				//pos will be valid in any case
				path[pos]='\0';
			}
			else //wont be needing this any more...
			{
				delete[] path;
				path=NULL;
			}
		}
		RegCloseKey(hKey); //no function with A suffix here
	}

	return path;
}


#endif



#include "directories.hpp"
#include "common/log.hpp"

char *Directories::user_conf=NULL;
char *Directories::user_data=NULL;
char *Directories::user_log=NULL;
char *Directories::inst_conf=NULL;
char *Directories::inst_data=NULL;

//NOTE: requires non-const string
bool Directories::Check_Path(char *path, dir_operation op)
{
	if (!path || path[0]=='\0') //doubt this kind of stupidity, but...
	{
		Log_Add(-1, "incorrect path for \"Check_Path\"!");
		return false;
	}

	if (op == READ)
	{
		if (!access(path, R_OK))
			return true;
		else
			return false;
	}
	else if (op == WRITE)
	{
		char test[strlen(path)+1]; strcpy(test, path);
		char tmp=0;
		int i=0;

		//just blindly try to create all missing directories...
		do
		{
			++i;

#ifndef _WIN32
			if (path[i] == '/' || path[i] == '\0')
#else
			//w32: also check backslashes
			if (path[i] == '\\' || path[i] == '/' || path[i] == '\0')
#endif
			{
				tmp=path[i];
				path[i]='\0';

				//create if missing
				if (access(path, F_OK))
				{
#ifndef _WIN32
					if (!mkdir(path, 0700)) //permission as specified by xdg
#else
					if (!mkdir(path)) //w32 lacks file permission mode, Ha!
#endif
						Log_Add(2, "created missing directory \"%s\"", path);
				}

				path[i]=tmp;

				//in case first of several slashes in a row:
				if (path[i] != '\0')
#ifndef _WIN32
					for (; path[i+1]=='/'; ++i);
#else
					for (; path[i+1]=='/' || path[i+1]=='\\'; ++i);
#endif
			}
		}
		while (path[i]!='\0');

#ifdef _WIN32
		//W_OK not supported on the Antiposix
		char tmpfile[strlen(path)+9];
		strcpy(tmpfile, path);
		strcat(tmpfile, "/tmpfile");
		FILE *fp = fopen(tmpfile, "w");
		if (fp) //could open tmpfile for writing!
		{
			fclose(fp);
			unlink(tmpfile); //don't care if works...
			return true;
		}
#else
		if (!access(path, W_OK))
			return true;
#endif
		else
			return false;
	}

	Log_Add(-1, "request for append-able directory for \"Check_Path\"");
	return false;
}

//concatenate path1+path2, check if ok and copy to target string
bool Directories::Try_Set_Path(char **target, dir_operation op,
		const char *path1, const char *path2)
{
	//something is wrong
	if (!path1 || !path2)
	{
		Log_Add(2, "Discarded incomplete path testing");
		return false;
	}

	char path[strlen(path1) +1 + strlen(path2) +1];
	strcpy(path, path1);
	strcat(path, "/");
	strcat(path, path2);

	if (Check_Path(path, op))
	{
		if (*target) delete *target;
		*target = new char[strlen(path)+1];
		strcpy(*target, path);

		return true;
	}
	else
		return false;
}

//concatenate path1+path2 for file path, and check if file is possible
bool Directories::Try_Set_File(dir_operation op,
				const char *path1, const char *path2)
{
	if (!path1 || !path2)
	{
		Log_Add(2, "Discarded incomplete file testing (read/write)");
		return false;
	}

	char file[strlen(path1) +1 +strlen(path2) +1];
	strcpy(file, path1);
	strcat(file, "/");
	strcat(file, path2);

	//this could require special testing/creation of path to file
	if (op == WRITE)
	{
		//NOTE: path2 is not system-generated, so assume user
		//specified with proper slashes instead of the w32 backslash
		//crap. Also: if path2 got no slashes, the explicitly
		//inserted one above will always provide a fallback

		//(similar to Directories::Init(), find leftmost-rightmost slash)
		int i;
		for (i=strlen(file); (i>0 && file[i]!='/'); --i);
		for (; i>0 && file[i-1]=='/'; --i);

		char path[i+1];
		memcpy(path, file, sizeof(char)*i);
		path[i]='\0';

		if (!Check_Path(path, WRITE))
		{
			Log_Add(0, "Unable to write/create path \"%s\"", path);
			return false;
		}
	}

	//use fopen() instead of access() for write/append check (and everything on Antiposix)
	int amode=0;
	const char *fmode=NULL;

	switch (op)
	{
		case READ:
#ifndef _WIN32
			amode=R_OK;
#else
			fmode="r";
#endif
			break;

		case WRITE:
			fmode="w";
			break;

		case APPEND:
			Log_Add(-1, "request for append-able directory for \"Check_Path\"");
			//amode, fmode are NULL, no checks will occur
			break;
	}

	bool viable=false;
	if (amode && !access(file, amode))
		viable=true;

	if (fmode) //if fmode, knows that amode=0 for sure
	{
		FILE *fp = fopen(file, fmode);
		if (fp)
		{
			//don't care about removing possibly created file (if "w")
			fclose(fp);
			viable=true;
		}
	}

	if (!viable)
	{
		if (op == WRITE) Log_Add(0, "Warning: unable to open \"%s\" for writing!", file);
		else Log_Add(2, "File \"%s\" did not exist", file);
		return false;
	}

	//else, we're ready to go!
	file_path = new char[strlen(file)+1];
	strcpy(file_path, file);
	return true;
}

//like above, but for appending (requires more quirks)
bool Directories::Try_Set_File_Append(const char *user, const char *inst, const char *path)
{
	if (!(user && path))
	{
		Log_Add(2, "Discarded incomplete file testing (append)");
		return false;
	}

	char upath[strlen(user)+1+strlen(path)+1];
	strcpy(upath, user);
	strcat(upath, "/");
	strcat(upath, path);

	int i;
	for (i=strlen(upath); (i>0 && upath[i]!='/'); --i);
	for (; i>0 && upath[i-1]=='/'; --i);

	if (i > 0)
	{
		char fullpath[i+1];
		memcpy(fullpath, upath, sizeof(char)*i);
		fullpath[i]='\0';

		if (!Check_Path(fullpath, WRITE))
		{
			Log_Add(0, "Unable to write/create path \"%s\"", path);
			return false;
		}
	}

	bool exists=access(upath, F_OK)? false: true;
	FILE *fp=fopen(upath, "ab");
	if (!fp)
	{
		Log_Add(0, "Warning: Unable to open \"%s\" for appending!", upath);
		return false;
	}

	//didn't exists in user dir before+got installed dir
	if (!exists && inst)
	{
		//does exist as installed?
		char ipath[strlen(inst)+1+strlen(path)+1];
		strcpy(ipath, inst);
		strcat(ipath, "/");
		strcat(ipath, path);

		FILE *ifp=fopen(ipath, "rb");
		if (ifp)
		{
			Log_Add(2, "Copying \"%s\" to \"%s\" before appending", ipath, upath);

			//TODO: move/copy to separate method (dedicated file copying)
			//NOTE: fopen() not as fast as open() (which is low-level posix)
			//(but - SURPRISE! - open() seems to cause problems on losedows...)
			char *buf = new char[COPY_BUFFER_SIZE];
			size_t s;
			while ((s = fread(buf, 1, COPY_BUFFER_SIZE, ifp)))
				fwrite(buf, 1, s, fp);

			delete[] buf;
			fclose (ifp);
		}
	}

	fclose(fp);
	file_path = new char[strlen(upath)+1];
	strcpy(file_path, upath);
	return true;
}


//
//Attempts to find suitable path to files, located around the executable (tries to figure out path
//from arg[0], and/or getmodulefilename on windows), if data and conf dirs are found, the path is
//returned (must be freed)
//
char *Directories::Try_Local_Path(const char *arg0)
{
	int pos; //find from the right
	char *path=NULL;
#ifdef _WIN32
	char *w32buf = new char[W32BUFSIZE];
	const char *wstr;

	//
	//try w32 api thing first, but arg0 if not working
	//
	if (GetModuleFileNameA(NULL, w32buf, W32BUFSIZE) != 0) //OK
	{
		Log_Add(2, "Path to program retrieved from w32 crap");
		w32buf[W32BUFSIZE-1]='\0'; //in case of insanity (see line 35 above)
		wstr=w32buf;
	}
	else //failure...
	{
		Log_Add(0, "Warning: unable to retrieve path to program (from w32 crap), trying arg0");
		wstr=arg0;
	}



	//
	//For all platforms: try finding path to executable from argv[0].
	//
	//Yes, I know realpath() exists, but:
	//1) if RC is called by name (in PATH), argv[0] will not be the path and realpath instead
	//   tries to give the path to a file in PWD
	//2) realpath() resolves symlinks, but I want to respect symlinked/moved executable
	//

	//note: checks for slashes too, but I don't think this can happen on w32?
	for (pos=strlen(wstr)-1; (pos>=0 && wstr[pos]!='\\' && wstr[pos]!='/'); --pos);
	for (; pos>0 && (wstr[pos-1]=='/' || wstr[pos-1]=='\\'); --pos);
#else

	//just check arg[0] on normal systems:
	for (pos=strlen(arg0)-1; (pos>=0 && arg0[pos]!='/'); --pos); //find from right
	for (; pos>0 && arg0[pos-1]=='/'; --pos); //in case several slashes in a row
#endif

	//pos should now be leftmost slash in rightmost group of slashes, or -1
	//pos==-1 means passed through all without finding separator
	if (pos>-1)  //found path to dir of executable, check if readable
	{
#ifdef _WIN32
		path = new char[pos+1];
		memcpy(path, wstr, sizeof(char)*pos);
#else
		//copy string to allow modification instead of modifying arg0
		path = new char[pos+1]; //room for s[0],s[1],s[2],...,s[pos-1],'\0'
		memcpy(path, arg0, sizeof(char)*pos);
#endif
		path[pos]='\0'; //terminate string to cut potential remaining path

		//"path" is the real stuff to check:
		if (
				//1) is data AND conf dirs next to executable?
				(Try_Set_Path(&inst_data, READ, path, "data") &&
				 Try_Set_Path(&inst_conf, READ, path, "conf")) ||

				//2) OR data AND conf in directory just above?
				(Try_Set_Path(&inst_data, READ, path, "../data") &&
				 Try_Set_Path(&inst_conf, READ, path, "../conf")) )
		{
			Log_Add(2, "Suitable directories located around the executable");
		}
		else
		{
			Log_Add(2, "Found no suitable directories around the executable");
			delete[] path;
			path=NULL;
		}
	}
#ifdef _WIN32
	delete[] w32buf;
#endif
	return path;
}


//
//Determines if local_path (if found) seems to be in an installed location (then avoid using it for
//portable mode). Or, if it was not even found, try determine suitable path to installed files
//
bool Directories::Try_Installed_Path(const char *local_path)
{
	bool allow_portable=false;

#ifdef _WIN32
	//try to get path to installed dir from registry
	char *w32regpath=RegToPath(HKEY_LOCAL_MACHINE, "SOFTWARE\\ReCaged", "Installed");

	if (!w32regpath)
		Log_Add(2, "No registry string found for path to installed copy (probably not installed)");
#endif

	//if found a (possibly writeable) path: on windows check if matches registry (if so, don't
	//write there), but on any other OS treat this as a sign of running portable (not installed)
	if (local_path)
	{
		allow_portable=true;

#ifdef _WIN32
		if (w32regpath && strcasecmp(w32regpath, local_path) == 0)
		{
			Log_Add(1, "Path to program matches registry string, running in installed mode");

			//prevents any further attempt to use these directories further as user writeable
			allow_portable=false;
		}
		else
#endif
		//if not, there is a path with valid dirs, and it might be used for portable mode...
		Log_Add(2, "Directories around executable might be suitable for portable mode");
	}
	else //no path found, fall back to potential installed locations
	{
		Log_Add(2, "Attempting to find installed directories");
		if (	Try_Set_Path(&inst_data, READ, DATADIR, "") &&
			Try_Set_Path(&inst_conf, READ, CONFDIR, "") )
			Log_Add(2, "Installed directories as specified during build configuration");
#ifdef _WIN32
		else if (w32regpath 	&& Try_Set_Path(&inst_data, READ, w32regpath, "data")
					&& Try_Set_Path(&inst_conf, READ, w32regpath, "conf") )
			Log_Add(2, "Installed directories as specified during installation (from registry)");
		else if (	(Try_Set_Path(&inst_data, READ, "data", "") &&
				Try_Set_Path(&inst_conf, READ, "conf", "")) ||
				(Try_Set_Path(&inst_data, READ, "../data", "") &&
				Try_Set_Path(&inst_conf, READ, "../conf", "")) )
				Log_Add(-1, "Found no suitable directories around executable or installed, but did in current working directory...");
#endif
		else
		{
			if (!inst_data)
				Log_Add(-1, "Found NO installed data directory!");

			if (!inst_conf)
				Log_Add(-1, "Found NO installed conf directory!");
		}
	}

#ifdef _WIN32
	//clean upp any remaining string allocations
	if (w32regpath)
		delete[] w32regpath;
#endif

	return allow_portable;
}

void Directories::Try_User_Path() 
{
	const char *var;

#ifdef _WIN32
	//on windows, try figure out path to user's "My Documents" 
	char *w32mydocs=NULL;
	//yes, this is not a recommended way of getting it, but it's the only one that
	//works across all different versions of windows.
	//TODO: replace with "SHGetFolderPath(NULL,CSIDL_PERSONAL,NULL,0,   result);" in the future!
	char *w32regpath = RegToPath(HKEY_CURRENT_USER, "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Shell Folders", "Personal");
	if (w32regpath) //ok
		w32mydocs=w32regpath;
	else //fallback to user home dir
	{
		Log_Add(-1, "Could not find path to \"My Documents\", using user home instead");
		w32mydocs=getenv("USERPROFILE");
	}
#endif
	//user conf
	if ( (var=getenv("XDG_CONFIG_HOME")) && Try_Set_Path(&user_conf, WRITE, var, "recaged") )
		Log_Add(2, "XDG path to user config directory");
	else if ( (var=getenv("HOME")) && Try_Set_Path(&user_conf, WRITE, var, ".config/recaged") )
		Log_Add(2, "fallback path to user config directory (XDG_CONFIG_HOME default)");
#ifdef _WIN32
	else if ( w32mydocs && Try_Set_Path(&user_conf, WRITE, w32mydocs, "My Games/ReCaged/conf") )
		Log_Add(2, "fallback, \"native\", path to user config directory (user Home or \"Documents\")");
	else if ( ((var=getenv("TMP")) || (var=getenv("TEMP"))) && Try_Set_Path(&user_conf, WRITE, var, "/recaged/conf"))
		Log_Add(-1, "found no suitable user config directory, using temporary directory instead!");
#else
	else if ( Try_Set_Path(&user_conf, WRITE, "/tmp/recaged/conf", "") )
		Log_Add(-1, "found no suitable user config directory, using \"/tmp/recaged/conf\" instead!");
#endif
	else
		Log_Add(-1, "found NO user config directory!");


	//user data
	if ( (var=getenv("XDG_DATA_HOME")) && Try_Set_Path(&user_data, WRITE, var, "recaged") )
		Log_Add(2, "XDG path to user data directory");
	else if ( (var=getenv("HOME")) && Try_Set_Path(&user_data, WRITE, var, ".local/share/recaged") )
		Log_Add(2, "fallback path to user data directory (XDG_DATA_HOME default)");
#ifdef _WIN32
	else if ( w32mydocs && Try_Set_Path(&user_data, WRITE, w32mydocs, "My Games/ReCaged/data") )
		Log_Add(2, "fallback, \"native\", path to user data directory (user Home or \"Documents\")");
	else if ( ((var=getenv("TMP")) || (var=getenv("TEMP"))) && Try_Set_Path(&user_data, WRITE, var, "/recaged/data"))
		Log_Add(-1, "found no suitable user data directory, using temporary directory instead!");
#else
	else if ( Try_Set_Path(&user_data, WRITE, "/tmp/recaged/data", "") )
		Log_Add(-1, "found no suitable user data directory, using \"/tmp/recaged/data\" instead!");
#endif
	else
		Log_Add(-1, "found NO user data directory!");


	//user log?
	if ( (var=getenv("XDG_STATE_HOME")) && Try_Set_Path(&user_log, WRITE, var, "recaged") )
		Log_Add(2, "XDG path to user log directory");
	else if ( (var=getenv("HOME")) && Try_Set_Path(&user_log, WRITE, var, ".local/state/recaged") )
		Log_Add(2, "fallback path to user log directory (XDG_STATE_HOME default)");
#ifdef _WIN32
	else if ( w32mydocs && Try_Set_Path(&user_log, WRITE, w32mydocs, "My Games/ReCaged") )
		Log_Add(2, "fallback, \"native\", path to user log directory (user Home or \"Documents\")");
	else if ( ((var=getenv("TMP")) || (var=getenv("TEMP"))) && Try_Set_Path(&user_log, WRITE, var, "/recaged"))
		Log_Add(-1, "found no suitable user log directory, using temporary directory instead!");
#else
	else if ( Try_Set_Path(&user_log, WRITE, "/tmp/recaged", "") )
		Log_Add(-1, "found no suitable user log directory, using \"/tmp/recaged\" instead!");
#endif
	else
		Log_Add(-1, "found NO user log directory!");

#ifdef _WIN32
	if (w32regpath)
		delete[] w32regpath;
#endif
}


//attempts to use the found readable directories (typically already checked to not seem to be an
//installed location), returns accordingly
bool Directories::Try_Portable_Path()
{
#ifdef _WIN32
	const char *var;
#endif

	if (	Try_Set_Path(&user_conf, WRITE, inst_conf, "") &&
		Try_Set_Path(&user_data, WRITE, inst_data, "") )
	{
		//don't need these anymore
		delete[] inst_conf; inst_conf=NULL;
		delete[] inst_data; inst_data=NULL;

		Log_Add(2, "Installed data and conf directories are writeable, using as user directories (portable mode)");


		//find good path for log. not checking in user home, since would just confuse
		//try: relative to found conf and data, then /tmp.
		if (	(Try_Set_Path(&user_log, WRITE, user_conf, "..")) ||
			(Try_Set_Path(&user_log, WRITE, user_data, "..")) )
			Log_Add(2, "Found suitable log directory above the others");
#ifndef _WIN32
		//in normal unixy systems, fall back to /tmp
		else if (Try_Set_Path(&user_log, WRITE, "/tmp/recaged", ""))
			Log_Add(-1, "Found no suitable log directory, using \"/tmp/recaged\" instead!");
#else
		//w32 got variable for tmp dir (check TMP and TEMP)...
		else if ( ((var=getenv("TMP")) || (var=getenv("TEMP"))) && Try_Set_Path(&user_log, WRITE, var, "/recaged"))
			Log_Add(-1, "Found no suitable log directory, using temporary directory (\"%s\") instead!", var);
#endif
		else
			Log_Add(-2, "Found NO user-writeable log directory!");

		return true; //great
	}

	//else, could not use for writing...
	return false;
}

//will only fail if user tries to override detection with weird paths (instead of ignoring them)
bool Directories::Init(	const char *arg0,
			bool installed_force, bool portable_force,
			const char *installed_override, const char *user_override)
{
	//make sure all values are null
	user_conf = NULL;
	user_data = NULL;
	user_log = NULL;
	inst_conf = NULL;
	inst_data = NULL;

	//avoid contradiction... should never occur anyway...
	if (installed_force && portable_force)
	{
		Log_Add(-2, "Contradictory installed/portable overrides");
		return false;
	}


	//
	//states
	//
	//suitable path to files located around executable (or NULL if not found)
	const char *local_path=NULL;
	//assume might be running in portable mode, until ruled out
	bool maybe_portable=true;




	//
	//installed data/conf dirs:
	//



	//
	//path to installed files (data+conf dirs) explicitly specified? (either when forcing
	//installed or portable mode)
	//
	if (installed_override) //no need to search, just verify user provided
	{
		if (	Try_Set_Path(&inst_data, READ, installed_override, "data") &&
			Try_Set_Path(&inst_conf, READ, installed_override, "conf") )
		{
			Log_Add(2, "Forced path to installed directory specified (\"%s\")", installed_override);
			local_path=installed_override;
		}
		else
		{
			Log_Add(-2, "Invalid forced path to installed directory (\"%s\")!", installed_override);
			return false;
		}
	}
	//
	//path to installed appears to be next to executable?
	//
	else
		local_path=Try_Local_Path(arg0);


	//
	//Unless portable is forced, refine/fallback for path above: if local_path is found, verify
	//it against known install path (mainly on windows, compared to registry) and if not found,
	//fetch the configured install path (registry on windows, or AC configure option on unixy)
	//
	if (!portable_force)
		maybe_portable = Try_Installed_Path(local_path);

	//path not needed any more, free if allocated (and not just copy of override arg)
	if (local_path && local_path!=installed_override)
		delete[] local_path;

	//
	//determine if running portable (not installed, user dirs same as installed)
	//


	//must be treated read only?
	if (installed_force)
		Log_Add(1, "Installed mode forced: treating installed directories as read-only");
	//no. if found both installation paths, check if writeable (or forced portable)
	//else if (w32 && GetReg strcmp not equals installed?, then also:
	//else if (found_inst && 	Try_Set_Path(&user_conf, WRITE, inst_conf, "") &&
	//			Try_Set_Path(&user_data, WRITE, inst_data, "") )
	else if (local_path && maybe_portable && Try_Portable_Path())
	{
		//we're done, just print the result and return:
		Log_Add(2, "Portable conf directory: \"%s\"", user_conf);
		Log_Add(2, "Portable data directory: \"%s\"", user_data);
		if (user_log) Log_Add(2, "Portable log directory: \"%s\"", user_log);

		return true;
	}
	//just print why forcing portable failed, and stop any further searching:
	else if (portable_force) {
		if (installed_override)
			Log_Add(-2, "Forced portable mode and path, but unable to write to specified path (\"%s\")!", installed_override);
		else
			Log_Add(-2, "Forced portable mode, but unable to find writeable directories!");

		return false;
	}
	else
		Log_Add(2, "Directories are not suitable for user files, finding user directories");



	//
	//Separate user directories (installed mode)
	//

	//forced user directory
	if (user_override)
	{
		//user conf+data+log
		if (	Try_Set_Path(&user_conf, WRITE, user_override, "conf") &&
			Try_Set_Path(&user_data, WRITE, user_override, "data") &&
			Try_Set_Path(&user_log, WRITE, user_override, ""))
		{
			Log_Add(1, "Alternate path to user directory specified (\"%s\")", user_override);
		}
		else
		{
			Log_Add(-2, "Forced path to user directory (\"%s\") is incorrect or not writeable", user_override);
			return false;
		}
	}
	//not forced user directory
	else
		Try_User_Path();


	//print results
	if (inst_conf) Log_Add(2, "Installed config directory: \"%s\"", inst_conf);
	if (inst_data) Log_Add(2, "Installed data directory: \"%s\"", inst_data);
	if (user_conf) Log_Add(2, "User config directory: \"%s\"", user_conf);
	if (user_data) Log_Add(2, "User data directory: \"%s\"", user_data);
	if (user_log) Log_Add(2, "User log directory: \"%s\"", user_log);

	return true; //all good, I think...
}

void Directories::Quit()
{
	delete user_conf; user_conf = NULL;
	delete user_data; user_data = NULL;
	delete user_log; user_log = NULL;

	delete inst_conf; inst_conf = NULL;
	delete inst_data; inst_data = NULL;
}


Directories::Directories()
{
	file_path=NULL;
}

Directories::~Directories()
{
	delete file_path;
}

const char *Directories::Find(const char *path,
		dir_type type, dir_operation op)
{
	delete file_path; file_path=NULL;

	const char *user=NULL, *inst=NULL;
	switch (type)
	{
		case CONFIG:
			Log_Add(2, "Locating config file \"%s\":", path);
			user=user_conf;
			inst=inst_conf;
			break;

		case DATA:
			Log_Add(2, "Locating data file \"%s\":", path);
			user=user_data;
			inst=inst_data;
			break;

		case LOG:
			Log_Add(2, "Locating log file \"%s\":", path);
			user=user_log;
			break;
	}

	switch (op)
	{
		case READ: //try user, then installed
			if ( Try_Set_File(op, user, path) )
				Log_Add(2, "Readable in user directory: \"%s\"", file_path);
			else if ( Try_Set_File(op, inst, path) )
				Log_Add(2, "Readable in installation directory: \"%s\"", file_path);
			else
				Log_Add(2, "Unable to find file \"%s\" for reading", path);
			break;

		case WRITE: //try user
			if ( Try_Set_File(op, user, path) )
				Log_Add(2, "Writeable in user directory: \"%s\"", file_path);
			else
				Log_Add(2, "Unable to resolve/create file \"%s\" for writing", path);
			break;

		case APPEND: //user, but copy from installed if needed/possible
			if ( Try_Set_File_Append(user, inst, path) )
				Log_Add(2, "Appendable in user directory: \"%s\"", file_path);
			else
				Log_Add(2, "Unable to resolve/create file \"%s\" for appending", path);
			break;
	}

	return file_path;
}

const char *Directories::Path()
{
	return file_path;
}

