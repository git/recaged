/*
 * ReCaged - a Free Software, Futuristic, Racing Game
 *
 * Copyright (C) 2009, 2010, 2011, 2012, 2014, 2023, 2024 Mats Wahlberg
 *
 * This file is part of ReCaged.
 *
 * ReCaged is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ReCaged is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ReCaged.  If not, see <http://www.gnu.org/licenses/>.
 */ 

//Autoconf results and settings
#include <config.h>

#include <stdarg.h>
#include <SDL/SDL_mutex.h>
#include "internal.hpp"
#include "log.hpp"

//for message box (and terminal colours if enabled) on windows
#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#endif

//default
static int stdout_verbosity = 1;
static bool logram = true;
static FILE *logfile = NULL;
static char *logbuffer=NULL;
static size_t bufferpos=0;
static size_t buffersize=0;
static SDL_mutex *logmutex = NULL;
static bool logenabled = false;

//verbosity indicators
static const char *indicator[] = {"\nERROR: ", "\nWARNING: ", "\n=> ", "-> ", " * "};

#ifdef LOGCOLOR
#ifdef _WIN32
static HANDLE hConsole;
static WORD origattributes;
#else
static bool stdcolor = false;
static bool errcolor = false;
#endif

/*
 * Message types and colours:
 * ERROR   (level=-2): red+underline
 * WARNING (level=-1): yellow+underline/bold
 * MAJOR   (level=0):  bold/bright cyan
 * MINOR   (level=1):  bold/bright green
 * DETAILS (level=2):  faint grey
 */

#ifdef _WIN32
static const WORD palette[] = {FOREGROUND_RED|FOREGROUND_INTENSITY,
	FOREGROUND_RED|FOREGROUND_GREEN|FOREGROUND_INTENSITY,
	FOREGROUND_GREEN|FOREGROUND_BLUE|FOREGROUND_INTENSITY,
	FOREGROUND_GREEN|FOREGROUND_INTENSITY, FOREGROUND_INTENSITY};
#else
//on most terminals, can use colour+bold+underline
static const char *palette_full[] = {"\033[0;1;4;31m", "\033[0;1;4;33m",
	"\033[0;1;36m", "\033[0;1;32m", "\033[0;2;37m"};
//on some terminals ("linux" tty's) underline causes issues
static const char *palette_safe[] = {"\033[0;1;31m", "\033[0;1;33m",
	"\033[0;1;36m", "\033[0;1;32m", "\033[0;2;37m"};
//choice of palette, adjusted at Init
static const char **palette=palette_safe;

//colours:
//31 = red
//32 = green
//33 = orange (yellow)
//34 = blue
//35 = purple
//36 = cyan
//37 = grey
//90 = dark grey
//91 = light red
//92 = light green
//93 = yellow
//94 = light blue
//95 = light purple
//96 = turquoise

//modifiers:
//0 	Reset
//1 	Bold or Bright
//2 	Faint
//3 	Italic
//4 	Underline
//5 	Slow blink
//6 	Rapid blink
//...

#endif
#endif


//text colors if not disabled
#ifdef LOGCOLOR

static int LAST_LEVEL=INT_MAX;
#ifdef _WIN32
#define STDCOLOR if (LAST_LEVEL!=level) {SetConsoleTextAttribute(hConsole, palette[level+2]); LAST_LEVEL=level;}
#define ERRCOLOR STDCOLOR
#else
#define STDCOLOR if (stdcolor && LAST_LEVEL!=level) {fputs(palette[level+2], stdout); LAST_LEVEL=level;}
#define ERRCOLOR if (errcolor && LAST_LEVEL!=level) {fputs(palette[level+2], stderr); LAST_LEVEL=level;}
#endif

#else
#define STDCOLOR
#define ERRCOLOR
#endif





//assume always called from a logging function (so mutex locked)
//size:minimum free space required (without \0), 0=increase by LOG_BUFFER_SIZE
static void IncreaseBuffer(size_t size)
{
	//increase size
	if (size==0) //once
		buffersize += LOG_BUFFER_SIZE;
	else if (buffersize-bufferpos >= size) //enough already
		return;
	else //increase until enough
		while (buffersize-bufferpos < size)
			buffersize += LOG_BUFFER_SIZE;

	char *old=logbuffer;
	logbuffer = new char[buffersize];

	//no earlier buffer
	if (!old)
		return;
	//else got existing buffer to copy
	memcpy(logbuffer, old, sizeof(char)*(bufferpos+1));

	//delete old buffer
	delete[] old;
}
//

//set up logging
void Log_Init()
{
	stdout_verbosity = 1; //make sure
#ifdef LOGCOLOR
	LAST_LEVEL = INT_MAX; //also make sure
#endif
	logfile = NULL;
	logbuffer = NULL;
	bufferpos = 0;
	buffersize = 0;

	logmutex = SDL_CreateMutex();

	//enable colour output if true terminal output
#ifdef LOGCOLOR
#ifdef _WIN32
	//TODO: always assume true on w32
	hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	//store original terminal settings
	CONSOLE_SCREEN_BUFFER_INFO info;
	GetConsoleScreenBufferInfo(hConsole, &info);
	origattributes = info.wAttributes;
#else
	//could probably assign directly, but technically different types...
	//TODO: should probably check errno...
	stdcolor=isatty(STDOUT_FILENO)? true: false;
	errcolor=isatty(STDERR_FILENO)? true: false;

	//check for terminals with good output style support
	char *term_var = getenv("TERM");
	if (	strncmp(term_var, "xterm", 5) ==0    || //xterm*
		strncmp(term_var, "rxvt", 4) ==0     || //rxvt*
		strncmp(term_var, "konsole", 7) ==0) {  //konsole*
		palette=palette_full; //with underline
	}
	else {
		palette=palette_safe; //only colour and bold
	}

#endif
#endif

	Log_RAM(true);
	logenabled=true;
	Log_Add(2, "Enabled logging system");

	//just for info
#ifdef LOGCOLOR
#ifndef _WIN32
	if (palette==palette_full) {
		//this one will typically only be visible in the log file
		Log_Add(2, "Terminal type \"%s\", assuming full support for colours, bold, underline", term_var);
	}
	else {
		Log_Add(1, "Terminal type \"%s\", assuming limited support for colours, bold", term_var);
	}
#endif
#else
	Log_Add(1, "Terminal colour output disabled during build");
#endif
}

void Log_Quit()
{
	if (!logenabled) {
		return;
	}

	Log_Add(2, "Disabling logging system");
	Log_File(NULL); //make sure is closed

	if (logbuffer) {
		delete[] logbuffer;
		logbuffer = NULL;
	}

	//reset terminal colours
#ifdef LOGCOLOR
#ifdef _WIN32
	SetConsoleTextAttribute(hConsole, origattributes);
#else
	if (stdcolor) {
		fputs("\033[0m", stdout);
	}
	if (errcolor) {
		fputs("\033[0m", stderr);
	}
#endif
#endif
	fflush(stdout);
	fflush(stderr);

	SDL_DestroyMutex(logmutex);

	logenabled=false;
}

void Log_RAM(bool ram)
{
	SDL_mutexP(logmutex); //changing variables might mess with running log append
	logram=ram;

	if (ram)
		Log_Add(2, "Enabled logging to RAM");
	else
		Log_Add(2, "Disabling logging to RAM");

	SDL_mutexV(logmutex); //should be fine from now on

}

bool Log_File(const char *file)
{
	SDL_mutexP(logmutex); //just make sure no logging while closing
	if (logfile)
	{
		if (file) Log_Add(2, "Closing current log file");
		else Log_Add(2, "File logging disabled");

		fclose(logfile);
		logfile=NULL;
	}
	SDL_mutexV(logmutex); //should be fine from now

	if (file)
	{
		if ((logfile=fopen(file, "w"))) {
			Log_Add(2, "Enabled logging to file \"%s\"", file);

			//if we got (possibly) log messages already stored:
			if (logram)
			{
				Log_Add(2, "Writing existing log (including this and the previous line!) to file...");
				fputs(logbuffer, logfile);
			}
		}
		else {
			Log_Add(-2, "Unable to open \"%s\" for logging", file);
			return false;
		}
	}

	return true;
}

void Log_Change_Verbosity(int v)
{
	stdout_verbosity+=v;

	if (stdout_verbosity<-2)
		stdout_verbosity=-2;
	else if (stdout_verbosity>2)
		stdout_verbosity=2;
}

//annoy the user on windoze, empty macro otherwise
#ifdef _WIN32
#define W32ERROR() MessageBoxA(NULL, "ERROR DURING LOG OUTPUT GENERATION!", "Error!", MB_ICONERROR | MB_OK);
#else
#define W32ERROR()
#endif

//simple calculation, reuse (search from current pos, bogus optimized)
#define UPDATEPOS() bufferpos=strlen(logbuffer+bufferpos)+bufferpos

//generate and append message from varargs
#define ARGPARSE() \
	va_list list; \
	size_t i; \
	while (1) \
	{ \
		va_start (list, text); \
		i=vsnprintf (logbuffer+bufferpos, buffersize-bufferpos, text, list); \
		va_end (list); \
		\
		/*if error*/ \
		if (i < 0) \
		{ \
			puts("ERROR DURING LOG OUTPUT GENERATION!"); \
			\
			/*and annoy the user on windoze*/ \
			W32ERROR() \
			SDL_mutexV(logmutex); \
			return; \
		} \
		\
		/*if ok*/ \
		if (i<buffersize-bufferpos) \
		{ \
			UPDATEPOS(); \
			break; \
		} \
		\
		/*lacking memory, try again with more*/ \
		IncreaseBuffer(0); \
	}



//print log message - if it's below or equal to the current verbosity level
void Log_Add (int level, const char *text, ...)
{
	//TODO: should probably check for error from fputs, putc, puts
	if (logram || logfile || level <= stdout_verbosity)
	{
		SDL_mutexP(logmutex); //make sure no conflicts

		//reset buffer position if not logging to ram
		if (!logram)
			bufferpos=0;

		//index to current line added to log
		int entry=bufferpos;

		//at least enough size to put indicator
		IncreaseBuffer(9); //9=enough for 8 chars and 1 \0

		//begin with verbosity indicator (+update position)
		strcpy(logbuffer+bufferpos, indicator[level+2]);
		UPDATEPOS();

		//parse variable arguments
		ARGPARSE();

		//similar to earlier, add newline
		IncreaseBuffer(2); //enough for \n and \0
		strcpy(logbuffer+bufferpos, "\n");
		UPDATEPOS();

		//write to targets (+add newling)
		if (logfile)
			fputs(logbuffer+entry, logfile);

		//special case
		if (level == -2)
		{
			ERRCOLOR
			//write to stderr instead of stdout (+some newlines to
			//make it visible)
			fputs(logbuffer+entry, stderr);


#ifdef _WIN32
			//and annoy the user on windoze
			//don't want the newlines in beginning and end...
			logbuffer[bufferpos]='\0'; //replace \n with \0...
			MessageBoxA(NULL, logbuffer+entry+1, "Error!", MB_ICONERROR | MB_OK);
			logbuffer[bufferpos]='\n'; //...and put back
#endif
		}
		else if (level <=stdout_verbosity) //normal case, if high enough verbosity
		{
			STDCOLOR
			fputs(logbuffer+entry, stdout);
		}

		SDL_mutexV(logmutex); //ok
	}
}

//two simple wrappers:
void Log_printf (int level, const char *text, ...)
{
	if (logram || logfile || level <= stdout_verbosity)
	{
		SDL_mutexP(logmutex);

		 //reset buffer position if not logging to ram
		if (!logram)
			bufferpos=0;

		//pointer to current line added to log
		int entry=bufferpos;

		ARGPARSE();

		if (logfile)
			fputs(logbuffer+entry, logfile);

		if (level == -2) {
			ERRCOLOR
			fputs(logbuffer+entry, stderr);
		}
		else if (level <=stdout_verbosity) {
			STDCOLOR
			fputs(logbuffer+entry, stdout);
		}
		SDL_mutexV(logmutex);
	}
}

//prints without verbosity indicator
void Log_puts (int level, const char *text)
{
	//no need to check "logram || logfile || level <= stdout_verbosity" - will not use costly ARGPARSE()
	SDL_mutexP(logmutex);
	if (logfile) fputs(text, logfile);
	if (logram) //just append to buffer (replicated part of ARGPARSE())
	{
		int l=strlen(text)+1; //size (with NULL)
		IncreaseBuffer(l);
		memcpy(logbuffer+bufferpos, text, sizeof(char)*l);
		UPDATEPOS();
	}

	if (level == -2) {
		ERRCOLOR
		fputs(text, stderr);
	}
	else if (level <= stdout_verbosity) {
		STDCOLOR
		fputs(text, stdout);
	}

	SDL_mutexV(logmutex);
}

