#
# ReCaged - a Free Software, Futuristic, Racing Game
#
# Copyright (C) 2023, 2024 Mats Wahlberg
#
# This file is part of ReCaged.
#
# ReCaged is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ReCaged is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ReCaged.  If not, see <http://www.gnu.org/licenses/>.
#

#
# RC_OPTIONS()
#
# Provide some configuration options
#

AC_DEFUN([RC_OPTIONS],
[

#allows disabling of manpage (generation and installation)
AC_ARG_ENABLE(
	[logcolor],
	[AS_HELP_STRING([--disable-logcolor],
			[Disable color output to terminal @<:@default=no@:>@])], [
		if test "x$enableval" != "xno"; then
			LOGCOLOR="yes"
		else
			LOGCOLOR="no"
		fi
		], [LOGCOLOR="yes"] )

#provide define to code if true
if test "$LOGCOLOR" = "yes"; then
	AC_DEFINE([LOGCOLOR], [1], [Print program log in color])
fi


#allows disabling of manpage (generation and installation)
AC_ARG_ENABLE(
	[manpage],
	[AS_HELP_STRING([--disable-manpage],
			[Do not generate or install manual page @<:@default=no@:>@])], [
		if test "x$enableval" != "xno"; then
			MANPAGE="yes"
		else
			MANPAGE="no"
		fi
		], [MANPAGE="yes"] )

#make accessible to doc/aminclude.am
AM_CONDITIONAL([MANPAGE], [test "$MANPAGE" = "yes"])

#allows disabling of ChangeLog.txt from git log generation
AC_ARG_ENABLE(
	[gitlog],
	[AS_HELP_STRING([--disable-gitlog],
			[Do not generate ChangeLog from git log @<:@default=no@:>@])], [
		if test "x$enableval" != "xno"; then
			GITLOGEN="yes"
		else
			GITLOGEN="no"
		fi
		], [GITLOGEN="yes"] )


AC_MSG_CHECKING(if ChangeLog from git has been disabled)
GITLOG="no"
if test "$GITLOGEN" = "no"; then
	AC_MSG_RESULT([yes, disabled by --disable-gitlog])
else
	AC_MSG_RESULT([no])

	AC_CHECK_FILE(["$srcdir/.git"], [GITDIR="yes"], [GITDIR="no"])
	AC_CHECK_PROG([GITEXIST], [git], "yes", "no")

	AC_MSG_CHECKING(if generating ChangeLog from git)
	if test "$GITEXIST" = "no"; then
		AC_MSG_RESULT([no, git is missing])
	elif test "$GITDIR" = "no"; then
		AC_MSG_RESULT([no, .git directory missing])
	else
		AC_MSG_RESULT([yes])
		GITLOG="yes"
	fi
fi

#make accessible to top Makefile
AM_CONDITIONAL([GITLOG], [test "$GITLOG" = "yes"])

])

